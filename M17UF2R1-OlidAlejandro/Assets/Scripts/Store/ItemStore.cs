using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ItemStore : MonoBehaviour
{
    public LootSO ItemSO;
    public Image ImageItemStore;
    public TMP_Text TextItemStore;
    public PlayerController PlayerController;
    public PlayerSO PlayerSO;
    public LootListSO StoreAvaiableItemsSO;
    public CanvasRenderer NoCoinsCanvas;
    public float counterNoCoinsMessage;
    public bool isNoCoinsMessage;


    public void InputBuyItem()
    {
        if (PlayerSO.Coins > ItemSO.GetPrice())
        {
            PlayerSO.Coins -= ItemSO.GetPrice();

            switch (ItemSO.GetID())
            {
                case 1:
                    PlayerController.Healing();
                    break;

                case 3:
                    PlayerController.TakeWeapon(StoreAvaiableItemsSO.findWeaponByName(ItemSO.GetName()));
                    break;
            }
        }
        else
        {
            ShowNoCoinsMessage();
        }
    }

    private void ShowNoCoinsMessage()
    {
        NoCoinsCanvas.gameObject.SetActive(true);
        isNoCoinsMessage = true;
    }

    private void Update()
    {
        if (isNoCoinsMessage)
        {
            counterNoCoinsMessage += Time.deltaTime;

            if (counterNoCoinsMessage >= 3f)
            {
                NoCoinsCanvas.gameObject.SetActive(false);
                isNoCoinsMessage = false;
            }
        }
    }
}
