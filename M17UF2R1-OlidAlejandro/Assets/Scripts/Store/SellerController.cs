using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SellerController : MonoBehaviour
{
    [SerializeField]
    Canvas Store;
    
    [SerializeField]
    PlayerSO PlayerSO;
    
    [SerializeField]
    bool isStoreOpen = false;
    
    [SerializeField]
    InputMaster InputMaster;

    [SerializeField]
    ItemStore ItemStore_1;

    [SerializeField]
    ItemStore ItemStore_2;
    
    [SerializeField]
    ItemStore ItemStore_3;

    [SerializeField]
    LootListSO StoreAvaiableItemsSO;

    private void Start()
    {
        InputMaster = new InputMaster();
        InputMaster.Gameplay.Enable();
    }

    private void Update()
    {
        if (isStoreOpen)
        {
            InputStoreExit();
        }
    }

    private void RefillStore()
    {
        ItemStore_1.ItemSO = StoreAvaiableItemsSO.GetRandomItem();
        ItemStore_2.ItemSO = StoreAvaiableItemsSO.GetRandomItem();
        ItemStore_3.ItemSO = StoreAvaiableItemsSO.GetRandomItem();

        ItemStore_1.ImageItemStore.GetComponentInChildren<Image>().sprite = ItemStore_1.ItemSO.GetSprite();
        ItemStore_2.ImageItemStore.GetComponentInChildren<Image>().sprite = ItemStore_2.ItemSO.GetSprite();
        ItemStore_3.ImageItemStore.GetComponentInChildren<Image>().sprite = ItemStore_3.ItemSO.GetSprite();

        ItemStore_1.TextItemStore.GetComponentInChildren<TMP_Text>().text = ItemStore_1.ItemSO.GetName();
        ItemStore_2.TextItemStore.GetComponentInChildren<TMP_Text>().text = ItemStore_2.ItemSO.GetName();
        ItemStore_3.TextItemStore.GetComponentInChildren<TMP_Text>().text = ItemStore_3.ItemSO.GetName();
    }



    private void InputStoreExit()
    {
        if (InputMaster.Gameplay.Dash.WasPressedThisFrame() && isStoreOpen)
        {
            CloseStore();
        }
    }

    private void CloseStore()
    {
        isStoreOpen = false;
        Store.gameObject.SetActive(false);
        GameManager.Instance.ResumeGame();
    }

    private void OpenStore()
    {
        RefillStore();
        isStoreOpen = true;
        Store.gameObject.SetActive(true);
        GameManager.Instance.PauseGame();
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (collision.GetComponent<PlayerController>().playerSO.isPurchasing)
            {
                OpenStore();
            }
        }
    }
}
