using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "ScriptableObject/Weapon")]
public class WeaponSO : LootSO
{
    [Header("Weapon Settings")]

    //[SerializeField]
    //int Size = 1;

    //[SerializeField]
    //[Range(1, 10)]
    //int AttackVelocity = 10;

    [SerializeField]
    [Range(0.1f, 10f)]
    float AttackCooldown = 1;

    public virtual void Attack(Transform transform) { }
    public virtual void TakeBullets() { }
    public virtual int GetCurrentNumBullets() { return 0;}

    public float GetAttackCoolDown()
    {
        return AttackCooldown;
    }
}
