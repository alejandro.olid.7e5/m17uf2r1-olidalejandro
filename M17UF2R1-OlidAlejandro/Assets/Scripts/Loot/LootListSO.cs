using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Loot List", menuName = "ScriptableObject/Loot List")]
public class LootListSO : ScriptableObject
{
    public List<LootSO> List;

    public LootSO GetRandomItem()
    {
        int randomNum = Random.Range(1, 101);
        List<LootSO> itemList = new List<LootSO>();
        foreach (LootSO item in List)
        {
            if (randomNum <= item.GetChance())
            {
                itemList.Add(item);
            }
        }

        return itemList[Random.Range(0, itemList.Count)];
    }

    public WeaponSO findWeaponByName(string name)
    {
        foreach (WeaponSO weapon in List)
        {
            if (weapon.name == name)
            {
                return weapon;
            }
        }
        return null;
    }
}
